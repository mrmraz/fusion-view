const quadrilateral = echarts.graphic.extendShape({
	shape: {},
	buildPath: function(ctx, shape) {
		ctx.moveTo(shape.A[0], shape.A[1]).lineTo(shape.B[0], shape.B[1]).
		lineTo(shape.C[0], shape.C[1]).lineTo(shape.D[0], shape.D[1]).closePath()
	}
})

echarts.graphic.registerShape('quadrilateral', quadrilateral)

function initRender(setting) {
	var info = setting.info
	var option = setting.option
	for ( var i in option.series ) {
		const barIndex = i
		// console.log(i)
		if ( !info[barIndex] ) {
			option.series[barIndex].renderItem = null
		} else {
			option.series[barIndex].renderItem = function(params,api) {
				const location = api.coord([api.value(0),0])
				return {
					type: 'group',
					children: [{
						//left
						type: 'quadrilateral',
						shape: {
							api,
							A: [location[0] + info[barIndex].xOffset, location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex]],
							B: [location[0] + info[barIndex].xOffset + info[barIndex].OC[0], location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex] + info[barIndex].OC[1]],
							C: [location[0] + info[barIndex].xOffset + info[barIndex].OC[0], location[1] + info[barIndex].yOffset[params.dataIndex] + info[barIndex].OC[1]],
							D: [location[0] + info[barIndex].xOffset, location[1] + info[barIndex].yOffset[params.dataIndex]],
						},
						style:{
							fill:info[barIndex].faceColors[0]
						}
					},{
						//right
						type:'quadrilateral',
						shape:{
							api,
							A: [location[0] + info[barIndex].xOffset, location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex]],
							B: [location[0] + info[barIndex].xOffset, location[1] + info[barIndex].yOffset[params.dataIndex]],
							C: [location[0] + info[barIndex].xOffset + info[barIndex].OB[0], location[1] + info[barIndex].yOffset[params.dataIndex] + info[barIndex].OB[1]],
							D: [location[0] + info[barIndex].xOffset + info[barIndex].OB[0], location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex] + info[barIndex].OB[1]],
						},
						style:{
							fill:info[barIndex].faceColors[1]
						}
					},{
						//top
						type:'quadrilateral',
						shape:{
							api,
							A: [location[0] + info[barIndex].xOffset, location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex]],
							B: [location[0] + info[barIndex].xOffset + info[barIndex].OB[0], location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex] + info[barIndex].OB[1]],
							C: [location[0] + info[barIndex].xOffset + info[barIndex].OB[0] + info[barIndex].OC[0], location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex] + info[barIndex].OB[1] + info[barIndex].OC[1]],
							D: [location[0] + info[barIndex].xOffset + info[barIndex].OC[0], location[1] + info[barIndex].yOffset[params.dataIndex] - info[barIndex].height[params.dataIndex] + info[barIndex].OC[1]],
						},
						style:{
							fill:info[barIndex].faceColors[2]
						}
					},]
				}
			}
		}	
	}
}


function changeSelected(setting, selected) {
	var stack = setting.stack
	var axisData = setting["option"]["xAxis"]["data"]
	
	//获取堆叠图中不同柱的数据总和的最大值，以此确定所有柱的高度
	var maxValue = 0
	// console.log(setting)
	// console.log(selected)
	for ( var i in axisData ) {
		for ( var j in stack ) {
			var temp = 0
			for ( var k in stack[j] ) {
				if ( !selected || selected[stack[j][k].name] ) {
					temp += stack[j][k].data[i]
				}
			}
			if ( maxValue < temp ) {
				maxValue = temp
			}
		}
	}
	
	//获取stackLength
	var stackLength = 0
	for ( var i in stack ) {
		for ( var j in stack[i] ) {
			if ( !selected || selected[stack[i][j].name] ) {
				stackLength++
				break
			}
		}
	}
	
	//创建info
	var newInfo = JSON.parse(JSON.stringify(setting.info))
	//生成option
	var newOption = JSON.parse(JSON.stringify(setting.option))
	//stack的编号
	var stackIndex = 0
	//bar的编号
	var barIndex = 0
    for ( var i in stack ) {
    	//初始化
		var heightTemp = []
		for ( var k in axisData ) { heightTemp.push(0) }
		
		var flag = false //判断这个stack是否有selected bar, true -> has
    	for ( var j in stack[i] ) {
	    	if ( !selected || selected[stack[i][j].name] ) {
	    		flag = true
	    		newInfo[barIndex].height = []
				newInfo[barIndex].yOffset = []
				newInfo[barIndex].xOffset = parseInt(setting.param1) * (stackIndex - (stackLength - 1)/2);
	    		for ( var k in axisData ) {
	    			var height = parseInt(setting.param2) * stack[i][j].data[k] / maxValue;
	    			newInfo[barIndex].height.push(height);
	    			newInfo[barIndex].yOffset.push(-heightTemp[k]);
	    			heightTemp[k] += height;
	    		}
	    	} else {
				newInfo[barIndex] = null
    		}
    		barIndex++;
    	}
		if ( flag ) { stackIndex++ }
    }
	newOption.legend.selected = selected
	var newSetting = {option: newOption, info: newInfo}
	// console.log(newSetting)
	return newSetting
}