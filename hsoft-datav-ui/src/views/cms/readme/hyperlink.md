# 超链接

## 一、配置

### 1、图层

选中该超链接组件，在操作界面右侧的“图层名称”处可修改组件的名称。（建议设置，方便后期组件管理）

### 2、超链接

- 路径：设置跳转页面地址；

- 新页面位置：设置跳转的页面打开位置，可设置为新页面、相同框架、父框架集、整个窗口；

- 超链接样式：可设置为文本或者图片样式；

### 3、文本类型

- 显示文字：超链接文字，如下图；

- 字体大小：超链接文字字体大小，可以调节改变，例如设置字体大小为 60，如下图；

- 字体名称：超链接文字字体名称，可设置为宋体、黑体、微软雅黑、Digital、Chunkfive、unidreamLED、
  Arial、Helvetica、sans-serif，例如设置字体名称为“宋体”，如下图；

- 字体装饰线：超链接文字装饰线，可设置为无、下划线、中划线，例如设置为下划线，如下图；

- 未访问链接颜色：未点击超链接时字体颜色，例如设置颜色为蓝色，如下图；

  <viewer>
  <img src="../image/hyperlink-1.png" width="60%">
  </viewer>

- 鼠标悬停颜色：鼠标悬停在超链接上时字体颜色，例如设置颜色为红色，如下图；

  <viewer>
  <img src="../image/hyperlink-2.png" width="60%">
  </viewer>

### 4、图片类型

- 图片：点击图片框在打开的图库页面中选择图片；

- 宽度：图片的宽度，可以调节改变，例如设置为 200，如下图；

- 高度：图片的高度，可以调节改变，例如设置为 200，如下图；

  <viewer>
  <img src="../image/hyperlink-3.png" width="40%">
  </viewer>

### 5、动画

选中该超链接组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：弹跳、渐隐渐显、
向右滑入、向左滑入、放缩、旋转、滚动。
