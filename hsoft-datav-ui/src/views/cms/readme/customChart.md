# 自定义组件

* 在编辑大屏页面时平台内的基本组件不满足与设计需求，这时候需要自定义图表组件，该功能是和echarts图表无缝结合的，可以选择自定义组件，在右侧设置栏点击自定义图表option区域，会出现在线图表编辑器，可以在编辑器上编写组件代码并运行调试，调试好后确认应用。
  ><viewer>
  >  <img src="../image/customChart-1.png" width="80%">
  >  <img src="../image/customChart-2.png" width="80%">
  ></viewer>

* 针对一些比较好的创新性强的，高可用的自定义组件我们可以上传到自定义组件库，以备今后在复用，上传时我们填写好自定义的图表名称，所属图表，所属组件，以及标签，是否公开等，公开的话平台内的所有用户都可以看到你自定义上传的组件，不公开的话则为私有的组件仅对自己可见。
  ><viewer>
  >  <img src="../image/customChart-3.png" width="80%">
  >  <img src="../image/customChart-4.png" width="80%">
  ></viewer>

* 在使用自定义组件时，在编辑区域选中自定义组件，在右侧配置区域点击自定义组件库，会弹出自定义组件库列表，在列表中选中要使用的自定义组件这时候就应用了所选的自定义组件了。
  ><viewer>
  >  <img src="../image/customChart-5.png" width="80%">
  >  <img src="../image/customChart-6.png" width="80%">
  ></viewer>