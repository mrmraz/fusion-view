# 时间范围

## 配置

- 图层名称：修改组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；
  <viewer>
    <img src="../image/timeframe.png" width="80%">
  </viewer>

- label内容/字号/字体/颜色/文字粗细：设置label有关项；
  <viewer>
    <img src="../image/timeframe-1.png" width="50%">  
    <img src="../image/timeframe-2.png" width="50%">
  </viewer>

- 宽度：输入框的宽度；
  <viewer>
    <img src="../image/timeframe-3.png" width="50%">
  </viewer>

- input的name：用于设置input的name属性，与后端参数的字段相对应，可根据该属性进行过滤查询；

- 日期格式：单选，包括「月范围」「日期范围」「日期时间范围」；
  <viewer>
    <img src="../image/timeframe-4.png" width="50%">
  </viewer>

- 绑定组件：绑定所要联动的组件；
  <viewer>
    <img src="../image/timeframe-5.png" width="50%">  
    <img src="../image/timeframe-6.png" width="50%">
  </viewer>

- 载入动画：弹跳、渐隐渐现、向右滑入、向左滑入、放缩、旋转、滚动；
