// 动画配置
export const animateOptions = [{
    value: '',
    label: '请选择'
},{
    value: 'bounce',
    label: '弹跳'
}, {
    value: 'fadeIn',
    label: '渐隐渐现'
}, {
    value: 'slideInLeft',
    label: '向右滑入'
}, {
    value: 'slideInRight',
    label: '向左滑入'
}, {
    value: 'zoomIn',
    label: '放缩'
}, {
    value: 'flip',
    label: '翻转'
}, {
    value: 'rollIn',
    label: '滚动'
},{
    value: 'twinkling',
    label: '闪烁'
},{
    value: 'leftSkew',
    label: '向左倾斜'
},{
    value: 'rightSkew',
    label: '向右倾斜'
}
]