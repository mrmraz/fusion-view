
import echarts from "echarts";

export default {
  data() {
    return {
      interactChart:null,
      staticDataValue:null,
      chartDiv:null,
      interactOption:null,
      interactData:null,

    };
  },
  methods: {
    runcode() {

      let option = (
          
        //里面为要处理的代码块
        eval(this.interactData) 
        
        )(
          this.interactOption,
          this.chartDiv
        );

      this.interactChart.setOption(option, true);

    },
    
  }
  

}