// echart图表绑定
export function linkChart(name, arrs, bindList,drawingList) {
    //图表不与条件类组件绑定
    let exclusion = ["input","timeframe", "select", "cascade","tab","textCheckBox","timeline","group"];
    
    let renderCharts = drawingList.filter(item => { 
      return bindList.indexOf(item.customId) > -1 && exclusion.indexOf(item.type) == -1
    })
    
    if(name != undefined && name != ''){
      //遍历绑定组件
      renderCharts.forEach(item => {
      
        //获取组件参数
        let requestParameters = item.chartOption.requestParameters;

        //如果已经绑定其他echart图则替换参数
        //一个echart图只能被一个echart图绑定
        if(requestParameters != ""){
          //拆分成数组
          let paramArr = requestParameters.split('&');
          let bindFlag = false;
          paramArr.pop();
          //遍历参数数组，判断是否已经被其他echart图绑定
          for(let i in paramArr){
            //获取参数值
            let index = paramArr[i].indexOf("=");
            let arr = paramArr[i].substring(index+1,paramArr[i].length);
            //判断参数值是否为json格式，只有echart图绑定参数为json格式
            if(isJSON(arr)){
              //如果已经被绑定
              //删除此参数，echart图只保留一个参数
              bindFlag = true;
              paramArr.splice(i,1,name + "=" + arrs)
              
            }
          }
          //如果没有绑定过，在参数数组中添加
          if(bindFlag == false){
            //如果为新名称参数直接拼在结尾
            paramArr.push(name + "=" + arrs);
          }
          //将数组重新按照&符号拼接为字符串
          requestParameters = '&' + paramArr.join("&")
          
        }else{
          //如果为新名称参数直接拼在结尾
          requestParameters += "&"+ name + "=" + arrs;
        }
        //判断参数是否已&符号开始，是则删除该符号
        if(requestParameters.indexOf('&') == 0){
          item.chartOption.requestParameters = requestParameters.substring(1, requestParameters.length) + "&timestamp="+new Date().getTime();
        }else{
          //给绑定组件重新赋值参数渲染组件
          item.chartOption.requestParameters = requestParameters + "&timestamp="+new Date().getTime();
        }
      
        
      });
    }
}


export function isJSON(str) {
  if (typeof str == 'string') {
      try {
          var obj=JSON.parse(str);
          if(typeof obj == 'object' && obj ){
              return true;
          }else{
              return false;
          }

      } catch(e) {
         
          return false;
      }
  }else{
    return false;
  }
    
}

//条件类组件绑定
export function bindChart(name, arr, bindList,drawingList) {
    let exclusion = ["input","timeframe", "select", "cascade","tab","textCheckBox","timeline","group"];
    let renderCharts = drawingList.filter(item => { 
      return bindList.indexOf(item.customId) > -1 && exclusion.indexOf(item.type) == -1
    })
    //遍历绑定组件
    renderCharts.forEach(item => {
      
        //获取组件参数
      let requestParameters = item.chartOption.requestParameters;

      //如果已经包含该名称的参数则替换
      if(requestParameters != "" && requestParameters.indexOf(name) != -1){
        
        //拆分成数组
        let paramArr = requestParameters.split('&');
        if(paramArr.length>1){
          paramArr.pop();
        }
       
        //获取到包含该名称的数组项
        for(let i in paramArr){
            if(paramArr[i].indexOf(name) != -1){
              //替换位=为当前内容
            paramArr.splice(i,1,name + "=" + arr)
            
          }
        }
          //将数组重新按照&符号拼接为字符串
        requestParameters = '&' + paramArr.join("&")
        
      }else{
        if(requestParameters != ""){
          let paramArr = requestParameters.split('&');
          if(paramArr.length>1){
            paramArr.pop();
          }
          requestParameters = '&' + paramArr.join("&")
        }
        //如果为新名称参数直接拼在结尾
        requestParameters += "&"+name + "=" + arr;
      }
      //判断参数是否已&符号开始，是则删除该符号
      if(requestParameters.indexOf('&') == 0){
        item.chartOption.requestParameters = requestParameters.substring(1, requestParameters.length) + "&timestamp="+new Date().getTime();
      }else{
        //给绑定组件重新赋值参数渲染组件
        item.chartOption.requestParameters = requestParameters + "&timestamp="+new Date().getTime();
      }
      
      
    });
}

//获取联动组件列表
export function getLinkChart(drawingList) {
  if (drawingList) {
    let chartList = JSON.parse(JSON.stringify(drawingList));
      for(let index in chartList){
        if(chartList[index].chartType == "group"){
            chartList.splice(index, 1)
        }
        
      }
    return chartList;
  } else {
    return [];
  }
}