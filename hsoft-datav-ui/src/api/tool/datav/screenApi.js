import request from '@/utils/request'

// 查询数据大屏列表
export function listScreen(query) {
  return request({
    url: '/datav/screen/list',
    method: 'get',
    params: query
  })
}

// 查询数据大屏详细
export function getScreen(screenId) {
  return request({
    url: '/datav/screen/' + screenId,
    method: 'get'
  })
}

// 新增数据大屏
export function addScreen(data) {
  return request({
    url: '/datav/screen',
    method: 'post',
    data: data
  })
}

// 修改数据大屏
export function updateScreen(data) {
  return request({
    url: '/datav/screen',
    method: 'put',
    data: data
  })
}

// 删除数据大屏
export function delScreen(screenId) {
  return request({
    url: '/datav/screen/' + screenId,
    method: 'delete'
  })
}

export function copyApply(screenId) {
  return request({
    url: '/datav/screen/copyApply/' + screenId,
    method: 'post'
  })
}

// 查询用户列表
export function getUserList() {
  return request({
    url: '/datav/screen/userlist',
    method: 'get'
  })
}

//分享大屏页，推送
export function shareScreen(data) {
  return request({
    url: '/datav/screen/shareScreen',
    method: 'post',
    data: data
  })
}

//分享大屏页，推送
export function sharePage(data) {
  return request({
    url: '/viewerLogin',
    method: 'post',
    data: data
  })
}

// 查询数据大屏详细
export function getPage(tokenId) {
  return request({
    url: '/getShareToken?id=' + tokenId,
    method: 'get'
  })
}

// 查询大屏模板列表
export function getTemplateList(query) {
  return request({
    url: '/datav/screen/templateList',
    method: 'get',
    params: query
  })
}