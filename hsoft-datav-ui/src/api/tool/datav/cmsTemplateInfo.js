import request from '@/utils/request'

// 查询cms模板列表
export function listTemplate(query) {
  return request({
    url: '/datav/cmsTemplate/list',
    method: 'get',
    params: query
  })
}

// 查询cms模板列表
export function userTemplateList(query) {
  return request({
    url: '/datav/cmsTemplate/user/list',
    method: 'get',
    params: query
  })
}

export function listHotTemplates(query) {
  return request({
    url: '/datav/cmsTemplate/hot',
    method: 'get',
    params: query
  })
}

// 发布cms模板
export function releaseTemplate(data) {
  return request({
    url: '/datav/cmsTemplate/release',
    method: 'put',
    data: data
  })
}


// 查询cms模板详细
export function getTemplate(id) {
  return request({
    url: '/datav/cmsTemplate/' + id,
    method: 'get'
  })
}

// 新增cms模板
export function addTemplate(data) {
  return request({
    url: '/datav/cmsTemplate',
    method: 'post',
    data: data
  })
}

// 修改cms模板
export function updateTemplate(data) {
  return request({
    url: '/datav/cmsTemplate/update',
    method: 'put',
    data: data
  })
}
// 复制cms模板
export function copyTemplate(id) {
  return request({
    url: '/datav/cmsTemplate/copyTemplate/' + id,
    method: 'post',
  })
}
// 删除cms模板
export function delTemplate(id) {
  return request({
    url: '/datav/cmsTemplate/' + id,
    method: 'delete'
  })
}

// 导出cms模板
export function exportTemplate(query) {
  return request({
    url: '/datav/cmsTemplate/export',
    method: 'get',
    params: query
  })
}

// 使用数+1
export function useCountIncrease(data) {
  return request({
    url: '/datav/cmsTemplate/useCountIncrease',
    method: 'put',
    data: data
  })
}

// 收藏数+1
export function starCountIncrease(data) {
  return request({
    url: '/datav/cmsTemplate/starCountIncrease',
    method: 'put',
    data: data
  })
}

// 收藏数-1
export function starCountDecrease(data) {
  return request({
    url: '/datav/cmsTemplate/starCountDecrease',
    method: 'put',
    data: data
  })
}

// 查看数+1
export function viewCountIncrease(data) {
  return request({
    url: '/datav/cmsTemplate/viewCountIncrease',
    method: 'put',
    data: data
  })
}