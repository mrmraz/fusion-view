import request from '@/utils/request'

// 查询用户个人统计信息
export function getUserPanel(username) {
  return request({
    url: '/datav/user/panel/group/' + username,
    method: 'get'
  })
}
//查询是否为模板
export function getTemplatePie(username) {
  return request({
    url: '/datav/user/panel/template/pie/' + username,
    method: 'get'
  })
}
//查询公开模板
export function getTemplateOpenPie(username) {
  return request({
    url: '/datav/user/panel/template/open/pie/' + username,
    method: 'get'
  })
}
//查询模板分类
export function getTemplateBar(username) {
  return request({
    url: '/datav/user/panel/template/bar/' + username,
    method: 'get'
  })
}